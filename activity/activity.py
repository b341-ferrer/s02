

def is_numeric(input_str):
    return input_str.isdigit() and int(input_str) > 0;

year = " ";

while (not is_numeric(year)):
	year = input ("Please enter a year : ");

if(int(year)%4==0):
	print (f"{year} is a leap year");
else:
	print (f"{year} is a not leap year");


rows = int (input ("Enter a number of rows : ")); 
columns = int (input ("Enter a number of columns : "));

for y in range(rows):
	asterisk = "";
	for x in range(columns):
		asterisk += " *";
	print (asterisk);
